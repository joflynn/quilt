import React from 'react';

export default function Settings(props) {

  const { settings, onChange } = props;

  function makeOptions(list) {
    return list.map(option => {
      return (
        <option key={option} value={option} style={{backgroundColor: option}}>
          {option}
        </option>
      );
    });
  }

  return (
    <div className="settings">
      <h2>Settings</h2>
      <div className="field">
        <label htmlFor="rows">Rows:</label>
        <input type="number" value={settings.rows} id="rows" name="rows" onChange={onChange} disabled />
      </div>
      <div className="field">
        <label htmlFor="cols">Columns:</label>
        <input type="number" value={settings.cols} id="cols" name="cols" onChange={onChange} disabled />
      </div>
      <div className="field">
        <label htmlFor="backgroundOne">Background One:</label>
        <select multiple id="backgroundOne" name="backgroundOne" onChange={onChange}>
          {makeOptions(settings.colors.backgroundOne)}
        </select>
      </div>
      <div className="field">
        <label htmlFor="backgroundTwo">Background Two:</label>
        <select multiple id="backgroundTwo" name="backgroundTwo" onChange={onChange}>
          {makeOptions(settings.colors.backgroundTwo)}
        </select>
      </div>
      <div className="field">
        <label htmlFor="foregroundOne">Foreground One:</label>
        <select multiple id="foregroundOne" name="foregroundOne" onChange={onChange}>
          {makeOptions(settings.colors.foregroundOne)}
        </select>
      </div>
      <div className="field">
        <label htmlFor="foregroundTwo">Foreground Two:</label>
        <select multiple id="foregroundTwo" name="foregroundTwo" onChange={onChange}>
          {makeOptions(settings.colors.foregroundTwo)}
        </select>
      </div>
      <div className="field checkbox">
        <input type="checkbox" checked={settings.swap} id="swap" name="swap" onChange={onChange} />
        <label htmlFor="swap">Swap Foreground Colors?</label>
      </div>
    </div>
  );
}
