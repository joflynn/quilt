import React from 'react';
import MediterraneanTile from './mediterraneanTile'

export default function Quilt(props) {
  const { settings, updateIndex } = props;
  const { rows, cols, map, swap } = settings;

  const list = Array.from({length: rows * cols}, (_, i) => i);
  const blocks = list.map(idx => {
    const x = idx % cols + 1;
    const y = Math.floor(idx / cols) + 1;
    return <MediterraneanTile key={idx} x={x} y={y} idx={idx} colors={settings.colors} cols={cols} rows={rows} indicies={map[idx]} swap={swap} updateIndex={updateIndex} />;
  });
  return (
    <div className="quilt-wrapper">
      <div className="border" />
        <div className="quilt">
          {blocks}
        </div>
      <div className="border" />
    </div>

  );
}
