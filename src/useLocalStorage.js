import { useState } from 'react';

export default function useLocalStorage(key, init) {
  const [stored, setStored] = useState(() => {
    try {
      const item = window.localStorage.getItem(key);
      return item ? JSON.parse(item) : init;
    } catch (error) {
      console.log(error);
      return init;
    }
  });

  function setValue(value) {
    try {
      const newValue = value instanceof Function ? value(stored) : value;
      setStored(newValue);
      window.localStorage.setItem(key, JSON.stringify(newValue));
    } catch (error) {
      console.log(error);
    }
  }

  return [stored, setValue];
}
