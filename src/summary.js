import React from 'react';
import { blockColors } from './mediterraneanHelper';

export default function Summary(props) {
  const { settings } = props;
  const { rows, cols, colors, swap, map } = settings;

  const blocks = map.map((block, idx) => {
    const x = idx % cols + 1;
    const y = Math.floor(idx / cols) + 1;
    return blockColors(x, y, rows, cols, swap, colors).map((group, index) => colors[group][block[index]]);
  });

  const triangles = blocks.map(block => {
    return [block[1], block[2]];
  }).flat();

  const hexagons = blocks.map(block => {
    return block[0];
  });

  const reducer = (acc, cur) => {
    if (acc[cur] === undefined) {
      acc[cur] = 0;
    }
    acc[cur] += 1;

    return acc;
  };

  const triangleCounts = triangles.reduce(reducer, {});
  const hexagonCounts = hexagons.reduce(reducer, {});

  const triangleTotal = Object.values(triangleCounts).reduce((acc, cur) => { return acc + cur }, 0);
  const hexagonTotal = Object.values(hexagonCounts).reduce((acc, cur) => { return acc + cur }, 0);

  const triangleSummary = Object.keys(triangleCounts).map(color => {
    return (
      <div key={color}>
        <div className="triangle" style={{borderColor: color}} title={color} />
        &times;
        {triangleCounts[color]}
      </div>
    );
  });

  const hexagonSummary = Object.keys(hexagonCounts).map(color => {
    return (
      <div key={color}>
        <div className="mediterranean hexagon right" style={{backgroundColor: color}}>
          <div className="top" />
          <div className="bottom" />
        </div>
        &times;
        {hexagonCounts[color]}
      </div>
    );
  });

  return (
    <div className="summary">
      <h2>Summary</h2>
      <div className="summary-list">
        <div>
          <h3>Triangles</h3>
          {triangleSummary}
          Total {triangleTotal}
        </div>
        <div>
          <h3>Hexagons</h3>
          {hexagonSummary}
          Total {hexagonTotal}
        </div>
      </div>
    </div>
  );
}
