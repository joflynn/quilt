import { BACKGROUND_ONE, BACKGROUND_TWO, FOREGROUND_ONE, FOREGROUND_TWO } from './consts';

export function blockColors(x, y, rows, cols, swap, colors) {
  
  let foregroundA = FOREGROUND_ONE;
  let foregroundB = FOREGROUND_TWO;
  if (swap) {
    foregroundA = FOREGROUND_TWO;
    foregroundB = FOREGROUND_ONE;
  }

  let backgroundGroup = BACKGROUND_ONE;
  let topGroup = foregroundA;
  let bottomGroup = foregroundB;

  if ((y % 2 === 1 && (x-1) % 4 < 2) || (y % 2 === 0 && (x-1) % 4 >= 2)) {
    backgroundGroup = BACKGROUND_TWO;
  }

  if (y % 2 === 1) {
    topGroup = foregroundB;
    bottomGroup = foregroundA;

    if (y === 1) {
      topGroup = BACKGROUND_ONE;
      bottomGroup = foregroundA;
    }
    if (x === 1) {
      topGroup = BACKGROUND_ONE;
    }
  } else {
    if (x === 1) {
      bottomGroup = BACKGROUND_ONE;
    }
  }
  if (y === rows) {
    bottomGroup = BACKGROUND_ONE;
  }

  if (x === cols) {
    if (cols % 2 === 0) {
      if (y % 2 === 0) {
        bottomGroup = BACKGROUND_ONE;
      } else {
        topGroup = BACKGROUND_ONE;
      }
    } else {
      if (y % 2 === 0) {
        topGroup = BACKGROUND_ONE;
      } else {
        bottomGroup = BACKGROUND_ONE;
      }
    }
  }

  return [
    backgroundGroup,
    topGroup,
    bottomGroup
  ];

}
