import React from 'react';
import { blockColors } from './mediterraneanHelper';

export default function MediterraneanTile(props) {
  const { idx, x, y, colors, rows, cols, indicies, swap, updateIndex } = props;

  const [backgroundGroup, topGroup, bottomGroup] = blockColors(x, y, rows, cols, swap, colors);
  const [backgroundIndex, topIndex, bottomIndex] = indicies;

  const backgroundColor = colors[backgroundGroup][backgroundIndex];
  const topColor = colors[topGroup][topIndex];
  const bottomColor = colors[bottomGroup][bottomIndex];
  


  let direction = "right";
  if ((y % 2 === 0 && x % 2 === 1) || (y % 2 === 1 && x % 2 === 0)) {
    direction = "left";
  }

  function onClick(event) {
    event.stopPropagation();
    const { target } = event;
    const { className } = target;

    switch (className) {
      case "top":
        updateIndex(idx, 1, (topIndex + 1) % colors[topGroup].length);
        break;

      case "bottom":
        updateIndex(idx, 2, (bottomIndex + 1) % colors[bottomGroup].length);
        break;

      default:
        updateIndex(idx, 0, (backgroundIndex + 1) % colors[backgroundGroup].length);
    }
  }

  const klass = `mediterranean block ${direction}`;
  return (
    <div className={klass} style={{gridRolumn: x, gridRow: y, backgroundColor: backgroundColor}} onClick={onClick}>
      <svg style={{fillColor: topColor}} onClick={onClick}>
        <path d="M 0,0 h 10 l -10, 10 z"/>
      </svg>
      <div className="top" style={{borderColor: topColor}} onClick={onClick} />
      <div className="bottom" style={{borderColor: bottomColor}} onClick={onClick} />
    </div>
  );
}
