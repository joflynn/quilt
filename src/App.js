import React from 'react';
import Quilt from './quilt';
import Settings from './settings';
import Summary from './summary';
import './App.css';
import useLocalStorage from './useLocalStorage';

function App() {

  const [settings, setSettings] = useLocalStorage("quiltSettings", {
    rows: 12,
    cols: 12,
    colors: {
      backgroundOne: ["#f9f9fb"],
      backgroundTwo: ["#f7f3e8", "#ede4db"],
      foregroundOne: ["#a39e98", "#969593", "#6c6964", "#60716b"],
      foregroundTwo: ["#b9cdcb", "#b4cca8", "#6ebc92", "#6ba394"]
    },
    map: [
      //      0,         1,         2,         3,         4,         5,         6,         7,         8,         9,        10,        11
      [0, 0, 0], [0, 0, 2], [0, 0, 3], [0, 0, 2], [0, 0, 2], [0, 0, 1], [0, 0, 1], [0, 0, 0], [1, 0, 0], [1, 0, 3], [0, 0, 0], [0, 0, 0], 
      [0, 1, 0], [0, 3, 1], [1, 1, 3], [1, 1, 2], [0, 3, 2], [0, 2, 2], [0, 1, 0], [0, 1, 2], [0, 1, 1], [0, 1, 0], [0, 0, 3], [1, 2, 0], 
      [0, 0, 0], [1, 1, 2], [0, 0, 1], [0, 0, 1], [1, 2, 3], [0, 2, 2], [0, 1, 3], [0, 1, 1], [0, 2, 0], [0, 1, 0], [0, 2, 0], [0, 0, 1], 
      [0, 1, 0], [0, 0, 3], [1, 0, 3], [0, 0, 0], [0, 3, 1], [0, 3, 2], [0, 2, 2], [0, 0, 2], [0, 2, 0], [0, 3, 1], [1, 0, 3], [0, 1, 0], 
      [1, 0, 0], [0, 3, 1], [0, 2, 1], [0, 1, 1], [1, 3, 0], [1, 2, 2], [0, 1, 0], [0, 1, 3], [0, 3, 3], [1, 1, 2], [0, 2, 3], [0, 0, 0], 
      [0, 0, 0], [0, 1, 3], [0, 3, 1], [0, 0, 2], [0, 3, 2], [0, 1, 1], [0, 1, 3], [0, 1, 3], [0, 2, 1], [0, 1, 3], [0, 0, 1], [0, 2, 0], 
      [1, 0, 2], [0, 3, 0], [0, 0, 1], [0, 0, 3], [1, 3, 0], [0, 3, 1], [0, 0, 1], [0, 2, 1], [1, 0, 2], [0, 2, 1], [0, 2, 1], [0, 0, 0], 
      [0, 0, 0], [0, 0, 3], [0, 1, 0], [0, 0, 1], [0, 2, 3], [0, 3, 3], [1, 1, 2], [0, 1, 0], [0, 3, 0], [0, 3, 1], [0, 3, 2], [1, 2, 0], 
      [0, 0, 1], [1, 2, 0], [0, 3, 1], [0, 0, 0], [1, 1, 1], [0, 1, 3], [0, 1, 1], [0, 1, 1], [0, 3, 2], [0, 1, 0], [0, 0, 0], [0, 0, 2], 
      [0, 1, 0], [0, 1, 0], [0, 1, 0], [1, 0, 0], [0, 0, 3], [0, 2, 2], [1, 0, 1], [1, 3, 2], [0, 1, 2], [0, 3, 2], [1, 1, 1], [1, 1, 0], 
      [0, 0, 1], [0, 3, 3], [0, 1, 2], [0, 1, 0], [1, 1, 1], [0, 2, 1], [0, 1, 2], [0, 3, 0], [0, 0, 0], [1, 0, 1], [0, 1, 3], [0, 0, 2], 
      [0, 1, 0], [0, 1, 0], [0, 0, 0], [0, 0, 0], [0, 3, 0], [0, 2, 0], [0, 0, 0], [0, 0, 0], [0, 1, 0], [0, 0, 0], [0, 1, 0], [0, 2, 0],
    ],
    swap: false 
  });

  function onChange(e) {
    const { target } = e;
    const { name, type, checked } = target;
    let value = target.value;
    if (type === "checkbox") {
      value = checked;
    }

    setSettings(settings => ({...settings, [name]: value }));
  }
  function updateIndex(blockIdx, idx, value) {
    const { map } = settings;
    const block = map[blockIdx];
    block[idx] = value;
    setSettings({
      ...settings, 
      map: [
        ...map.slice(0, blockIdx),
        block,
        ...map.slice(blockIdx + 1)
      ]
    });
  }

  return (
    <div className="App">
      <Quilt settings={settings} updateIndex={updateIndex} />
      <Summary settings={settings} />
      <Settings settings={settings} onChange={onChange} />
    </div>
  );
}

export default App;
